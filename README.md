# What is this? #

This is a simple and lightweight container providing DNS via bind/named. It is based on the latest alpine image.

_**Note**: This image has been designed with my personal needs in mind!_

# How to #

_You'll find a docker-compose YAML file on the Bitbucket repository. I suggest taking a look at that._

On the first start the `entrypoint.sh` will check for existing configuration files in `/data/bind/etc`. If there are none, it will copy its default configuration there. Depending on how you intend to use this container, I'd advise to map a volume for the data directory `/data/bind`.

# Security #

This container will by default run as non-root user `named`. As a consequence, `bind` will bind to port 5353 TCP and UDP. To access the DNS service with default ports you'll have to map ports 53/tcp and 53/udp to their '5353 counterparts'.

