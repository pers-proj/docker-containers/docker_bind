#!/bin/sh

set -e

# populate default bind configuration if it does not exist
if [ $(ls -1 ${DATA_DIR}/etc | wc -l) -eq 0 ]; then
  cp -r /bind_default/* ${DATA_DIR}/etc
fi

# launch named
$(which named) -u named -g -p $BIND_PORT 

